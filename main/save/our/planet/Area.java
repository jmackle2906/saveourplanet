package save.our.planet;

import java.util.ArrayList;

public class Area {


    ArrayList<Tile> areaTiles = new ArrayList<Tile>();
    private String AreaName;


    public ArrayList<Tile> getAreaTiles() {
        return areaTiles;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        AreaName = areaName;
    }

    public void addTile(Tile tile) {

        areaTiles.add(tile);

    }
    public boolean isMonopolisedBy(Player player){
//TODO add error handling in the case of no tiles being set and add a test for the true class
//TODO maybe test the error handling
        for (Tile tile : areaTiles) {
            if (tile.isOwned()) {
                if (!player.equals(tile.getOwner()) ) {
                    return false;
                }
            } else {
                return false;
            }
        }


        return true;

    }


//    public void buyTile(Player playerName, Tile tile) {
//
//        tile.buyTile(playerName);
//        int oldResources =playerName.getPlayerResources();
//
//
//
//    }
}
