package save.our.planet;

import java.util.ArrayList;


public class Board {

    public static final int MAX_EXTENSION_COUNT = 3;
    public static final int StartTileAllowance = -20;

    public Board() {


    }

    public ArrayList<Area> areas = new ArrayList<Area>();


    private int numberOfPlayers;

    public ArrayList<Player> getPlayers() {
        return players;
    }

    //private Player[] players = new Player[numberOfPlayers];
    private ArrayList<Player> players = new ArrayList<Player>();



    public void newGame() {

        Area nothingTiles = new Area();
        Area area1 = new Area();
        Area area2 = new Area();
        Area area3 = new Area();
        Area area4 = new Area();

        nothingTiles.setAreaName("rest");
        area1.setAreaName("Agriculture");
        area2.setAreaName("Solar energy");
        area3.setAreaName("Clean Water");
        area4.setAreaName("Reduce waste");

        Tile startTile = new Tile();
        Tile tileSix = new Tile();

        Tile biofuel = new Tile();
        Tile sustainablelogging = new Tile();
        Tile seaweedCowFeed = new Tile();

        Tile solarPanels = new Tile();
        Tile sustainableBatteries = new Tile();

        Tile watertreatment = new Tile();
        Tile bioplastic = new Tile();
        Tile pesticideFreeGMO = new Tile();

        Tile lowPowerRecycling = new Tile();
        Tile labGrownBurgers = new Tile();

        startTile.addNameAndIndex("Start tile",0);
        tileSix.addNameAndIndex("Phantom Zone",6);
        biofuel.addNameAndIndex("Biofuel", 1);
        sustainablelogging.addNameAndIndex("Sustainable Logging", 2);
        seaweedCowFeed.addNameAndIndex("Seaweed Cow Feed", 3);
        solarPanels.addNameAndIndex("Solar Panels", 4);
        sustainableBatteries.addNameAndIndex("Sustainable Batteries", 5);
        watertreatment.addNameAndIndex("Water Treatment", 7);
        bioplastic.addNameAndIndex("Bioplastic", 8);
        pesticideFreeGMO.addNameAndIndex("Pesticide Free GMO", 9);
        lowPowerRecycling.addNameAndIndex("Low Power Recycling", 10);
        labGrownBurgers.addNameAndIndex("Lab Grown Burgers", 11);


        startTile.setPrice(0);
        tileSix.setPrice(0);

        biofuel.setPrice(50);
        sustainablelogging.setPrice(60);
        seaweedCowFeed.setPrice(70);

        solarPanels.setPrice(50);
        sustainableBatteries.setPrice(70);

        watertreatment.setPrice(70);
        bioplastic.setPrice(80);
        pesticideFreeGMO.setPrice(90);

        lowPowerRecycling.setPrice(120);
        labGrownBurgers.setPrice(140);

        nothingTiles.addTile(startTile);
        nothingTiles.addTile(tileSix);

        area1.addTile(biofuel);
        area1.addTile(sustainablelogging);
        area1.addTile(seaweedCowFeed);

        area2.addTile(solarPanels);
        area2.addTile(sustainableBatteries);

        area3.addTile(watertreatment);
        area3.addTile(bioplastic);
        area3.addTile(lowPowerRecycling);

        area4.addTile(pesticideFreeGMO);
        area4.addTile(labGrownBurgers);

        this.areas.add(nothingTiles);
        this.areas.add(area1);
        this.areas.add(area2);
        this.areas.add(area3);
        this.areas.add(area4);


    }


    public ArrayList<Area> getAreas() {
        return areas;
    }


    public int setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        return numberOfPlayers;
    }

    public int getNumberOfPlayers() {

        return this.numberOfPlayers;

    }

    public ArrayList<Player> setPlayerNames(String[] playerNames) {

		ArrayList<Player> players = new ArrayList<Player>();
		int i = 0;
		for (String eachName : playerNames) {
			@SuppressWarnings("deprecation")
			Integer x = new Integer(i+1);
			Player player = new Player();
			player.setPlayername("Player "+x+", "+eachName);
			players.add(player);
			i++;
		} 
		this.players = players;
		return players;

	}

    public String[] getPlayerNames() {
        int numberOfPlayers = players.size();
        String[] playernames = new String[numberOfPlayers];
        int i = 0;
        for (Player player : players) {

            playernames[i] = player.getPlayername();
            i++;
        }
        return playernames;
    }

    public ArrayList<Tile> getAllTiles() {

        ArrayList<Tile> allTiles = new ArrayList<Tile>();

        for (Area area : this.areas) {
            for (Tile tile : area.getAreaTiles()) {
                allTiles.add(tile);


            }


        }
        return allTiles;

    }

    public Tile getTile(int tileIndex) {
        Tile outtile = null;
        for (Area area : this.areas) {
            for (Tile tile : area.getAreaTiles()) {

                if (tile.getTileIndex() == tileIndex) {
                    outtile = tile;
                }


            }


        }
        return outtile;

    }

    public void setPlayerResources(int i) {

        for (Player player : this.players) {

            player.setPlayerResources(i);


        }


    }

    public int getPlayerResources(String playerName) {
        int outValue = 0;
        for (Player player : this.players) {
            if ( player.getPlayername().equals(playerName)) {
                outValue = player.getPlayerResources();
            }

        }

        return outValue;
    }

    public void changePlayerResources(String playerName, int changevalue) {

        for (Player player : this.players) {
            if (player.getPlayername() == playerName) {
                int newValue = player.getPlayerResources() + changevalue;
                player.setPlayerResources(newValue);
            }

        }

    }

    public int checkPlayerPosition(Player playerName) {
        int position = -1;
        for (Player player : this.players) {
            if (player.getPlayername().equals(playerName.getPlayername())) {
                position = player.getPlayerPosition();
            }

        }
        return position;
    }

    public void changePlayerposition(Player choseplayer, int change) {

        String playerName = choseplayer.getPlayername();
        for (Player player : this.players) {
            if (player.getPlayername().equals( playerName)) {

                int position = player.getPlayerPosition();
                int newposition = position + change;
                if (newposition >= 12) {
                    player.chargePlayer(StartTileAllowance);
                    //System.out.println("you have passed the start tile and have been awarded: "+StartTileAllowance);
                    System.out.println("You have passed the start tile and have been awarded: "+(StartTileAllowance*-1));
                    newposition = newposition - 12;
                }
                player.setPlayerPosition(newposition);

            }
        }
    }

    public Tile getPlayerTilePosition(Player playerName) {

        Tile outTile = null;
        for (Player player : this.players) {
            if (player.getPlayername().equals( playerName.getPlayername())) {

                outTile = this.getTile(player.getPlayerPosition());
            }
        }
        return outTile;

    }


    public boolean checkIfPositionIsOwned(Player playerName) {

        Tile tile = this.getPlayerTilePosition(playerName);
        return tile.isOwned();
    }

    public Area getAreaByTile(int tileIndex) {

        Area outArea = null;
        for (Area area : this.areas) {
            for (Tile tile : area.getAreaTiles()) {

                if (tile.getTileIndex() == tileIndex) {
                    outArea = area;
                }


            }


        }
        return outArea;


    }

    public void purchaseTile(Player playerName) {

        Tile tile = this.getPlayerTilePosition(playerName);
        tile.buyTile(playerName);

    }


    public Player searchForPlayerByName(String playerName) {
        Player outName = null;

        for (Player player : this.players) {
            if (player.getPlayername().equals( playerName)) {
                outName = player;
            }

        }
        return outName;
    }


    public boolean isAreaMonopolisedBy(Area area, Player player) {
        return area.isMonopolisedBy(player);
    }

    public ArrayList<Tile> getPlayerOwnedTiles(Player player) {
        ArrayList<Tile> out = new ArrayList<Tile>();

        for (Area area : this.areas) {

            for (Tile tile : area.getAreaTiles()) {
                if (tile.getOwner() == player) {
                    out.add(tile);
                }

            }

        }
        return out;


    }

    public ArrayList<Area> getUserMonopolisedAreas(Player player) {

        ArrayList<Area> out = new ArrayList<Area>();

        for (Area area : this.areas) {
            if (this.isAreaMonopolisedBy(area, player) == true) {
                out.add(area);
            }

        }
        return out;


    }

    public void buildExtension(Tile tile) {
        tile.buildExtension();

    }

    public int countExtensionsOnTile(Tile tile) {

        return tile.getExtensionCount();

    }

    public boolean allExtensionsAreBuilt(Area area) {
        for (Tile tile : area.getAreaTiles()) {
            if (tile.getExtensionCount() < MAX_EXTENSION_COUNT) {
                return false;
            }
        }

        return true;
    }

    public boolean playerLost() {

    for(Player player: players){
        if (player.getPlayerResources()<=0){
            return true;
        }
    }
    return false;

    }

    public boolean playerIsOnActionableTile(Player player) {

        if(getPlayerTilePosition(player).getPrice()!=0){

            return true;

        }
        return false;

    }

    public void chargePlayerForTileTheAreOn(Player player){
        for(Tile tile:getAllTiles()){
            if(player.getPlayerPosition()==tile.getTileIndex()){
                tile.chargeNoneOwner(player);
            }
        }
    }
    public void purchaseTilePlayerIsOn(Player player){
        for(Tile tile:getAllTiles()){
            if(player.getPlayerPosition()==tile.getTileIndex()){
                tile.buyTile(player);
            }
        }

    }


    public boolean doesPlayerHaveAnyFullyownedAreas(Player player) {

        for(Area area: areas){
            if (isAreaMonopolisedBy(area,player)){
                return true;
            }
        }
    return false;
    }

    public ArrayList<Tile> getALltilesThatExtensionCanBeBuiltOnForPlayer(Player player){
        ArrayList<Tile> out=new ArrayList<Tile>();
        for(Area area: getUserMonopolisedAreas(player)){
            for (Tile tile: area.getAreaTiles()){

                if(countExtensionsOnTile(tile)<3){
                    out.add(tile);
                }

                }
            }
        return out;



    }

    public ArrayList<Tile> getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(Player player){
        ArrayList<Tile> out=new ArrayList<Tile>();


        for(Area area: getUserMonopolisedAreas(player)){

            boolean check =true;
            for (Tile tile:area.getAreaTiles()){
                if(countExtensionsOnTile(tile)<3){
                    check = false;
                }
            }
            for (Tile tile: area.getAreaTiles()){



                if(countExtensionsOnTile(tile)==3) {
                    if (check) {
                        out.add(tile);
                    }
                }
            }
        }
        return out;



    }


    public boolean doesPlayerHaveAnyUpgradesToMake(Player player) {

        for(Area area: getUserMonopolisedAreas(player)){
            for(Tile tile:area.getAreaTiles()){
                if (tile.getExtensionCount()<4){
                    return true;
                }
            }

        }
        return false;


    }

    public Player getPlayerByName(String playerName) {

        Tile outTile = null;
        for (Player player : this.players) {
            if (player.getPlayername().equals(playerName)) {
                return (player);
            }
        }
        return null;

    }

}

