package save.our.planet;

public class Player {


    private String playername;
    private int playerPosition = 0;
    private int playerResources;


    public int getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(int playerPosition) {
        this.playerPosition = playerPosition;
    }

    public int getPlayerResources() {
        return playerResources;
    }

    public void setPlayerResources(int playerResources) {
        this.playerResources = playerResources;
    }

    public void chargePlayer(int amountToSubtract) {
        int old = this.getPlayerResources();
        this.playerResources = old - amountToSubtract;
    }

    public String getPlayername() {
        return playername;
    }

    public void setPlayername(String playername) {
        this.playername = playername;
    }


}
