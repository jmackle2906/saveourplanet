package save.our.planet;

import java.util.Scanner;

public class consoleSpecificCommands {
	static Scanner scanner = new Scanner(System.in);
	
    public static Boolean wouldYouLikeToPurchaseATile() {
        
        System.out.println("Would you like to purchase the tile you landed on [y/n]");
        String answer = scanner.nextLine();
        if (answer.equals("y") || answer.equals("Y")) {
            return true;
        }
    return false;
    }


    public static Boolean wouldYouLikeToUpgradeATile() {
        //Scanner scanner = new Scanner(System.in);
        //System.out.println("would you like to purchase the tile you landed on [y/n]");
        System.out.println("Would you like to upgrade a tile? y/n");
        String answer = scanner.nextLine();
        if (answer.equals("y") || answer.equals("Y")) {
            return true;
        }
        return false;
    }
    
  public static Boolean wouldYouLikeToViewYourProperties() {
    	
        System.out.println("Would you like to hear the properties you own? [y/n]");
        String answer = scanner.nextLine();
        if (answer.equals("y") || answer.equals("Y")) {
            return true;
        }
        return false;
    }
    
}
