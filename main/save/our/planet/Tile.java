package save.our.planet;

public class Tile {

    private String tileName;
    private int tileIndex;
    private int price;

    public void setNumberofExtensions(int numberofExtensions) {
        this.numberofExtensions = numberofExtensions;
    }

    private int numberofExtensions=0;
    private Player owner;
    private boolean ownedBoolean =false;
    public Player getOwner() {
        return owner;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }



    public void addNameAndIndex(String name,int index) {
        this.tileName= name;
        this.tileIndex=index;
    }


    public String getName(){
        return tileName;
    }


//    public void setTileIndex(int index){
//        this.tileIndex=index;
//    }


    public int getTileIndex() {
        return this.tileIndex;
    }

    public Boolean isOwned() {

        return this.ownedBoolean;
    }

    public void buyTile(Player player){
        this.owner =player;
        this.ownedBoolean=true;
        player.chargePlayer(this.price);
    }

    public int getCostOfNextUpgrade(){
        if(this.numberofExtensions<3) {
            return (this.numberofExtensions + 1) * (this.price / 10);
        }
        if (this.numberofExtensions==3){
            return (this.numberofExtensions + 1) *2* (this.price / 10);
        }
        System.out.println("Sorry, no extensions can be purchased");
        return 0;
    }

    public void buildExtension() {
        Player player = this.owner;
        if(this.numberofExtensions<3) {
            player.chargePlayer(this.getCostOfNextUpgrade());
            this.numberofExtensions = this.numberofExtensions + 1;
        } else {
            if (this.numberofExtensions==3){
                player.chargePlayer(this.getCostForFinalUpgrade());
                this.numberofExtensions = this.numberofExtensions + 1; }
        }
    }
    public int getExtensionCount() {
        return this.numberofExtensions;
    }

    public int getCostForFinalUpgrade() {
        return getCostOfNextUpgrade()*2;
//        this.numberofExtensions=this.numberofExtensions*2;
    }
    public int getPriceforNoneOwners(){
        return (price/10)*(numberofExtensions+1)*3;
    }
    public void chargeNoneOwner(Player noneOwner){
        noneOwner.chargePlayer(getPriceforNoneOwners());
        owner.chargePlayer(-getPriceforNoneOwners());

    }
/*    public void sellEverything(){
        this.ownedBy.chargePlayer(-this.getSellPrice());
        this.ownedBoolean=false;
        this.numberofExtensions=0;
    }
    public void sellExtension(){
        this.ownedBy.chargePlayer(-(this.getCostOfNextUpgrade()/2));
        this.numberofExtensions=this.numberofExtensions-1;
    }
    public int getSellPrice(){
        return this.price/(2/this.numberofExtensions+1);
    }
*/
}
