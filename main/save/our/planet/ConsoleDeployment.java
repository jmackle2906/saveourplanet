package save.our.planet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class ConsoleDeployment {

    public static final int PLAYER_RESOURCES = 500;
    Scanner scanner = new Scanner(System.in);
	
    int numberOfPlayers;
    String[] inNames;



    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    //public void setNumberOfPlayers(int numberOfPlayers) {
    public void setNumberOfPlayers() throws InterruptedException {
    	try {
    		System.out.println("Hello and welcome to Eco Warriors! Get ready to save the world!");

    		while(numberOfPlayers < 2 || numberOfPlayers > 4){
    			
    			System.out.println("Please say the number of Players between 2 - 4: ");
    	
    			numberOfPlayers = scanner.nextInt();
    		}
    		
    	} catch(Exception ex){
    		System.out.println("Invalid number of players, players has been defaulted to 2");
    		numberOfPlayers = 2;
    	}
    	Thread.sleep(600);
        this.numberOfPlayers = numberOfPlayers;
    }
   /* public String[] getInNames() {
        return inNames;
    }*/

    public void setInNames(String[] inNames) {
        this.inNames = inNames;
    }


    public void run() {

        //Scanner scanner = new Scanner(System.in);

        Board board = new Board();
        board.newGame();
        if (checkNumberOfPlayers(getNumberOfPlayers())) {
            board.setNumberOfPlayers(numberOfPlayers);
            board.setPlayerNames(inNames);
            board.setPlayerResources(PLAYER_RESOURCES);
            boolean gameStop = board.playerLost();
            do {
                for (Player player : board.getPlayers()) {

                   	System.out.println("It is your turn " + player.getPlayername());
					System.out.println("You have " + player.getPlayerResources() + " thousand dollars.");
                    
                    
                    if(consoleSpecificCommands.wouldYouLikeToViewYourProperties()==true){
						showOwnedProperties(board.getPlayerOwnedTiles(player));
						}	
                    
                    int roll = rollDice();
                    board.changePlayerposition(player, (roll));
                    System.out.println("You have moved " + roll + " places to " + board.getPlayerTilePosition(player).getName());
                    landingoptions( board, player);
                    
                	if (player.getPlayerResources() < 1) {
						gameStop = true;
						break;
					}
                    
                   	System.out.println(player.getPlayername() + " you have  "
            							+ board.getPlayerResources(player.getPlayername()) + " thousand dollars remaining");

                    upgradingOptions(scanner, board, player);

                    System.out.println("Are you ready to continue?[Press Enter]. To quit please say quit [Enter quit]");
					String option = scanner.nextLine();


                    if(player.getPlayerResources()<1) {
                    	gameStop = true;
                    	break;
                    }
                    
                    //if (option.equals("quit()")) {
                    if (option.equals("quit")) {
                        gameStop = true;
                        break;
                    }

                }


            } while (gameStop == false);
            endOfGameScores(board);
        }
    }

    public void endOfGameScores(Board board) {
        ArrayList<Integer> scores = getSortedScores(board);

        int i = 0;
        for(Integer score:scores){
            System.out.print("\n");
            System.out.print((i+1)+": ");
            printPlayerByScore(board, score);
            i++;
        }
    }

    private void printPlayerByScore(Board board, Integer score) {
        for(Player player:board.getPlayers()){
            if(score.equals(player.getPlayerResources())){
                System.out.print(player.getPlayername()+" .Your final score is "+player.getPlayerResources()+" ,");

            }
        }
    }

    private ArrayList<Integer> getSortedScores(Board board) {
        ArrayList<Integer> scores = new ArrayList<Integer>();
        for(Player player:board.getPlayers()){

            scores.add(player.getPlayerResources());

        }
        Collections.sort(scores);
        return scores;
    }


    public void upgradingOptions(Scanner scanner, Board board, Player player) {
        if(board.doesPlayerHaveAnyFullyownedAreas(player)){
            if(board.doesPlayerHaveAnyUpgradesToMake(player)) {
            	System.out.println("Would you like to upgrade a tile? y/n");
                String answerText=scanner.nextLine();
                if(answerText.toLowerCase().equals("y")||answerText.toLowerCase().equals("n")) {
                    decisionWouldPlayerLikeToUpgradeTile(answerText, board, player);
                }
                return;
                //decisionWouldPlayerLikeToUpgradeTile(scanner, board, player);
            }
        }
    }

    //private void decisionWouldPlayerLikeToUpgradeTile(Scanner scanner, Board board, Player player) {
    private void decisionWouldPlayerLikeToUpgradeTile(String answerText, Board board, Player player) {
        if (YesOrno(answerText)) {
            //if (upgradeExtensionsOrAreas(player, board, scanner)) {
            if (upgradeExtensionsOrAreas(player, board, answerText)) {
                ArrayList<Tile> toBeFullyUpgraded = getAreasToBeFullyUpgraded(player, board);
                //upgradeEitherExtensionsOrFinal(scanner,board,toBeFullyUpgraded);
                upgradeEitherExtensionsOrFinal(answerText,board,toBeFullyUpgraded);
            } else {
                ArrayList<Tile> tiles = board.getALltilesThatExtensionCanBeBuiltOnForPlayer(player);
                //upgradeEitherExtensionsOrFinal(scanner, board, tiles);
                upgradeEitherExtensionsOrFinal(answerText, board, tiles);
            }
        }
    }

    //public boolean upgradeExtensionsOrAreas(Player player, Board board, Scanner scanner) {
    public boolean upgradeExtensionsOrAreas(Player player, Board board, String answerText) {
        ArrayList<Tile> areas = board.getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(player);
        if(areas.isEmpty()==false) {
            if (board.getALltilesThatExtensionCanBeBuiltOnForPlayer(player).isEmpty()==false) {
                System.out.println("Would you like to fully upgrade  a tile or build an extension [y/n]");
                //return YesOrno(scanner);
                return YesOrno(answerText);
            }
            return true;
        }else{
            return false;
        }
    }

    public ArrayList<Tile> getAreasToBeFullyUpgraded(Player player, Board board) {
        ArrayList<Tile> tile = new ArrayList<Tile>();
        for (Tile area : board.getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(player)) {
                tile.add(area);
        }
        return tile;
    }

    //public void upgradeEitherExtensionsOrFinal(Scanner scanner, Board board, ArrayList<Tile> tiles) {
    public void upgradeEitherExtensionsOrFinal(String answerText, Board board, ArrayList<Tile> tiles) {
        boolean acceptableInput= false;
        int upgradeChoice= -20;
        do {
            for (Tile tile : tiles) {
                System.out.println("To upgrade " + tile.getName() + "for :" + tile.getCostOfNextUpgrade() + "   Please enter " + tile.getTileIndex());

            }
            //TODO need a try catch especially for string input
            //TODO this is rather important
            try {
                upgradeChoice = Integer.parseInt(scanner.nextLine());
                for (Tile tile : tiles) {
                    if (upgradeChoice == tile.getTileIndex()) {
                        acceptableInput = true;
                    }
                }
            }catch (Exception e){
                System.out.println("please enter an integer from the options provided. \n \n");
            }
        }while (acceptableInput==false);
        Tile upgradeTile = board.getTile(upgradeChoice);
        upgradeTile.buildExtension();
    }

    public int rollDice() {
        int roll1 = Die.roll();
        System.out.println("Dice 1 landed on " +roll1);
        int roll2 = Die.roll();
        System.out.println("Dice 2 landed on " +roll2);
        return roll1+roll2;
    }

    public boolean YesOrno(String answerText) {
// public boolean YesOrno(Scanner scanner) {
// String answer =scanner.nextLine();
// boolean answer = consoleSpecificCommands.wouldYouLikeToUpgradeATile();
        if (answerText.equals("y") || answerText.equals("Y")){
            return true;
        }
        return false;
    }

    public boolean landingoptions(Board board, Player player) {
    	//Check if Player is on the 'Start' Tile
        if (board.getPlayerTilePosition(player).getOwner()==player){
            return true;
        }

        if(board.getPlayerTilePosition(player).getTileIndex()==0){

            return true;
        }
        //Check if Player is on the 'Phantom Zone' Tile
        if(board.getPlayerTilePosition(player).getTileIndex()==6){
        	return true;
        }
        //Check if the Player is on an Actionable Tile
        if(board.playerIsOnActionableTile(player)){

            landedOnActionableTile(board, player);
            return true;


        }
        return  false;
    }

    public void landedOnActionableTile( Board board, Player player) {
        if(board.checkIfPositionIsOwned(player)){
            board.chargePlayerForTileTheAreOn(player);
            System.out.println("You landed on someone else's property. You have been fined "+board.getPlayerTilePosition(player).getPriceforNoneOwners());
            return;
        }else{
            System.out.println("This tile costs "+board.getPlayerTilePosition(player).getPrice()+" thousand dollars");
            boolean answer = consoleSpecificCommands.wouldYouLikeToPurchaseATile();
            //System.out.print(answer);
                if(answer){
                    board.purchaseTile(player);
                    System.out.println("Congratulations!! You have purchased this property!");
                    System.out.println();
                    return;
                }
            };


        }


//    public String[] nameEachPlayer(Scanner scanner, int numberOfPlayers) {
//        String [] playerNames = new String[numberOfPlayers];
//        for(int i=0;i<numberOfPlayers;i++){
//            System.out.println("please enter the name of player \" "+(i+1));
//            String inName=scanner.nextLine();
//            System.out.println("\" ");
//            playerNames[i] = "Player "+(i+1)+" "+inName;
//            System.out.println(playerNames[i]);
//        }
//        return playerNames;
//    }

    public String[] setPlayerNames(int numberOfPlayers) {
        String [] playerNames = new String[numberOfPlayers];
        String inName = "";
        scanner.nextLine();
        for(int i=0;i<numberOfPlayers;i++){
        	
        	int tempPlayerNum = i+1;
            System.out.println("Please enter the name of player "+(tempPlayerNum));
            inName=scanner.nextLine();
            System.out.println("Player " + tempPlayerNum + " : " + inName);
            playerNames[i] = inName;
            //scanner.next();
            //System.out.println("\" ");
            //playerNames[i] = "Player "+(i+1)+" "+inName;
            //System.out.println(playerNames[i]);
        }
        System.out.println("Begin Game!");
        System.out.println();
        return playerNames;
    }
    
    public Boolean checkNumberOfPlayers(int numberOfPlayers) {

    if (numberOfPlayers>4|numberOfPlayers<2){
        return false;
    };
        return true;
    }
    
    
    
	public void showOwnedProperties (ArrayList<Tile> tiles) {
		
		if (tiles.size() > 0) {
			System.out.print("You own: ");
			for (Tile tile : tiles) {
				System.out.print(tile.getName()+", ");
			}
		} else {
			System.out.println("You do not own any properties yet");
		}
	}

}
