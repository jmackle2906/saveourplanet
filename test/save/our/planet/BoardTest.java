package save.our.planet;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;


public class BoardTest {

/*    @Before
    public void setUp() throws Exception {
    }*/

    public Board setup1() {
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        return board;
    }


    public Board setup2() {
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(2);
        String[] names = {"a", "b"};
        board.setPlayerNames(names);
        return board;
    }

    public Board setup3() {
        Board board = setup2();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        Tile tile = board.getPlayerTilePosition(player);
        tile.buyTile(player);
        board.changePlayerposition(player, 11);
        Tile tile1 = board.getPlayerTilePosition(player);
        tile1.buyTile(player);
        board.changePlayerposition(player, 11);
        Tile tile2 = board.getPlayerTilePosition(player);
        tile2.buyTile(player);
        return board;
    }

    @Test
    public void testNewGameCreatesAreas() throws Exception {
        Board board = new Board();
        board.newGame();
        int expected = 5;
        int actual = board.getAreas().size();
        assertEquals(expected, actual);

    }

    @Test
    public void testSetNumberOfPlayers() throws Exception {

        Board board = new Board();
        board.setNumberOfPlayers(4);

        int actual = board.getNumberOfPlayers();
        int expected = 4;
        assertEquals(actual, expected);


    }

    @Test
    public void testSetPlayerNames() throws Exception {
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        String[] expected = board.getPlayerNames();
        String actual = "Player 1, a";
        assertEquals(actual, expected[0]);

    }

    @Test
    public void testSettingtiles() throws Exception {
        Board board = new Board();
        board.newGame();

        ArrayList<Tile> areaTiles = new ArrayList<Tile>();

        int actual = board.getAllTiles().size();
        int expected = 12;
        assertEquals(actual, expected);


    }

    @Test
    public void testGetTileByNumber() {
        Board board = new Board();
        board.newGame();
        Tile testTile = board.getTile(1);
        String expected = "biofuel";
        String actual = testTile.getName();

        assertEquals(actual.toLowerCase(), expected);
    }

    @Test
    public void testcheckPlayerResources() {
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        board.setPlayerResources(500);
        int actual = board.getPlayerResources("Player 1, a");
        int expected = 500;
        assertEquals(actual, expected);
    }

    @Test
    public void testchangePlayerResources() {
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        board.setPlayerResources(500);
        board.changePlayerResources("Player 1, a", -50);
        int actual = board.getPlayerResources("Player 1, a");
        int expected = 450;

    }

    @Test
    public void testcheckPlayerPosition() {
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        Player player = board.searchForPlayerByName("Player 1, a");
        int actual = board.checkPlayerPosition(player);
        int expected = 0;
        assertEquals(actual, expected);

    }

    @Test
    public void testchangePlayerPosition() {
        Board board = this.setup1();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        int actual = board.checkPlayerPosition(player);
        int expected = 3;
        assertEquals(actual, expected);

    }

    @Test
    public void testCheckPlayerTilePosition() {

        Board board = this.setup1();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        Tile tile = board.getPlayerTilePosition(player);
        String actual = tile.getName();
        actual =actual.toLowerCase();
        String expected = "seaweed cow feed";
        assertEquals(actual, expected);
    }

    @Test
    public void testGetAreaByTile() {
        Board board = setup2();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        Tile tile = board.getPlayerTilePosition(player);
        int tileindex = tile.getTileIndex();
        Area area = board.getAreaByTile(tileindex);
        String actual = area.getAreaName();
        String expected = "Agriculture";
        assertEquals(actual, expected);
    }


    @Test
    public void testCheckIfTileIsOwned() {

        Board board = setup2();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        boolean expected = board.checkIfPositionIsOwned(player);

        assertFalse(expected);
    }

    @Test
    public void testPurchaseTile() {
        Board board = setup2();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        board.purchaseTile(player);
        boolean expected = board.checkIfPositionIsOwned(player);
        assertTrue(expected);

    }

    @Test
    public void testTestchargePlayer() {
        Board board = setup2();
        Player player = board.searchForPlayerByName("Player 1, a");
        board.changePlayerposition(player, 3);
        Tile tile = board.getPlayerTilePosition(player);


        int before = player.getPlayerResources();
        tile.buyTile(player);
        int after = player.getPlayerResources();
        assertNotEquals(before, after);


    }

//    @Test
//    public void testChargePlayerOnPurchasedTile() {
//
//
//    }

    @Test
    public void testCheckIfAreaIsMonopolised() {

        Board board = setup2();
        Player player = board.searchForPlayerByName("Player 1, a");

        board.changePlayerposition(player, 3);
        Tile tile = board.getPlayerTilePosition(player);
        tile.buyTile(player);
        board.changePlayerposition(player, 11);
        Tile tile1 = board.getPlayerTilePosition(player);
        tile1.buyTile(player);
        board.changePlayerposition(player, 11);
        Tile tile2 = board.getPlayerTilePosition(player);
        tile2.buyTile(player);
        Area area = board.getAreaByTile(tile.getTileIndex());
        boolean expected = board.isAreaMonopolisedBy(area, player);
        assertTrue(expected);

    }
    @Test
    public void testCheckIfAreaIsNotMonopolised() {

        Board board = setup2();

        Player player = board.searchForPlayerByName("Player 1, a");
        Player player2 = board.searchForPlayerByName("Player 2, b");
        board.changePlayerposition(player, 3);
        Tile tile = board.getPlayerTilePosition(player);
        tile.buyTile(player);
        board.changePlayerposition(player, 11);
        Tile tile1 = board.getPlayerTilePosition(player);
        tile1.buyTile(player);
        board.changePlayerposition(player2, 1);
        Tile tile2 = board.getPlayerTilePosition(player2);
        tile2.buyTile(player2);
        Area area = board.getAreaByTile(tile.getTileIndex());
        boolean expected = board.isAreaMonopolisedBy(area, player);
        assertFalse(expected);

    }

    @Test
    public void testGetUserOwnedTiles() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        ArrayList<Tile> tile = board.getPlayerOwnedTiles(player);
        int expected = tile.size();
        int actual = 3;
        assertEquals(actual, expected);


    }

    @Test
    public void testGetUserMonopolisedAreas() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        ArrayList<Area> areas = board.getUserMonopolisedAreas(player);
        int expected = areas.size();
        int actual = 1;
        assertEquals(actual, expected);

    }


    @Test
    public void testBuildExtension() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        Tile tile = board.getPlayerTilePosition(player);
        Area area = board.getAreaByTile(tile.getTileIndex());
        board.buildExtension( tile);
        int expected = board.countExtensionsOnTile(tile);
        int actual = 1;
        assertEquals(expected, actual);

    }

    @Test
    public void testCheckIfAllExtensionsAreBuilt() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        Tile tile = board.getPlayerTilePosition(player);
        Area area = board.getAreaByTile(tile.getTileIndex());
        for (Tile buytile : area.getAreaTiles())
            for (int i = 0; i < 3; i++) {
                buytile.buildExtension();
            }
        boolean expected = board.allExtensionsAreBuilt(area);
        assertTrue(expected);
    }
    @Test
    public void testCheckIfAllExtensionsAreNotBuilt() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        Tile tile = board.getPlayerTilePosition(player);
        Area area = board.getAreaByTile(tile.getTileIndex());
        for (Tile buytile : area.getAreaTiles())
            for (int i = 0; i < 2; i++) {
                buytile.buildExtension();
            }
        boolean expected = board.allExtensionsAreBuilt(area);
        assertFalse(expected);
    }




    @Test
    public void testBuildNewEcoFriendlySite() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        Tile tile = board.getPlayerTilePosition(player);
        Area area = board.getAreaByTile(tile.getTileIndex());
        for (Tile buytile : area.getAreaTiles())
            for (int i = 0; i < 3; i++) {
                buytile.buildExtension();
            }

        tile.buildExtension();
        int actual = tile.getExtensionCount();
        int expected = 4;
        assertEquals(actual, expected);


    }
    @Test
    public void testTilePriceWasSetCorrectly(){
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        Tile tile = board.getPlayerTilePosition(player);
        tile.setPrice(50);
        int expected =50;
        int actual = tile.getPrice();
        assertEquals(expected,actual);
    }


    @Test
    public void testThatTheNumberOFPlayersIsCorrectInArrayList(){
        Board board = setup3();
        int actual =board.getPlayers().size();
        int expected =2;
        assertEquals(actual,expected);

    }
    @Test
    public void testHasAPlayerNotLost(){
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"Player 1, a"};
        board.setPlayerNames(names);
        board.setPlayerResources(500);
        assertFalse(board.playerLost());
    }
    @Test
    public void testPlayerLost(){
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        board.setPlayerResources(0);
        assertTrue(board.playerLost());
    }


    @Test
    public void testHasAPlayerLost(){
        Board board = new Board();
        board.newGame();
        board.setNumberOfPlayers(1);
        String[] names = {"a"};
        board.setPlayerNames(names);
        board.setPlayerResources(500);
        assertFalse(board.playerLost());
    }
        @Test
    public void testPlayerIsOnActionableTile(){
            Board board = setup3();
            Player player = board.searchForPlayerByName("Player 1, a");
            assertTrue(board.playerIsOnActionableTile(player));

    }

    @Test
    public void testPlayerIsNotOnActionableTile() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 2, b");
        assertFalse(board.playerIsOnActionableTile(player));

    }

    @Test
    void testchargePlayerForTileTheAreOn() {
        Board board = setup3();
        Player player = board.searchForPlayerByName("Player 1, a");
        Player playerb = board.searchForPlayerByName("Player 2, b");
        Tile tile = board.getPlayerTilePosition(player);
        board.changePlayerposition(player,8);

        board.getPlayerTilePosition(player).buyTile(playerb);
        board.getPlayerByName("Player 1, a").setPlayerResources(500);
        board.getPlayerTilePosition(player).setPrice(100);
        board.chargePlayerForTileTheAreOn(player);

        int expected =470;
        int actual = board.getPlayerByName("Player 1, a").getPlayerResources();
        assertEquals(expected,actual);



    }

    @Test
    void testpurchaseTilePlayerIsOn() {
        Board board = setup3();

        Player playerb = board.searchForPlayerByName("Player 2, b");

        board.changePlayerposition(playerb,8);

        board.purchaseTilePlayerIsOn(playerb);
        Player actual =board.getPlayerTilePosition(playerb).getOwner();

        assertEquals(playerb,actual);

    }

    @Test
    void testdoesPlayerHaveAnyFullyownedAreastrue() {
        Board board = setup1();
        board.getTile(1).buyTile(board.getPlayerByName("Player 1, a"));
        board.getTile(2).buyTile(board.getPlayerByName("Player 1, a"));
        board.getTile(3).buyTile(board.getPlayerByName("Player 1, a"));

        assertTrue(board.doesPlayerHaveAnyFullyownedAreas(board.getPlayerByName("Player 1, a")));


    }
    @Test
    void testdoesPlayerHaveAnyFullyownedAreasfalse() {
        Board board = setup1();
        assertFalse(board.doesPlayerHaveAnyFullyownedAreas(board.getPlayerByName("Player 1, a")));

    }

    @Test
    void testgetALltilesThatExtensionCanBeBuiltOnForPlayer() {
        Board board = setup3();
        int expected =3;
        int actual = board.getALltilesThatExtensionCanBeBuiltOnForPlayer(board.getPlayerByName("Player 1, a")).size();
        assertEquals(expected,actual);
    }

    @Test
    void testgetALltilesThatFinalUpgradeCanBeBuiltOnForPlayer() {
        Board board = setup3();
        board.getTile(1).setNumberofExtensions(3);
        board.getTile(2).setNumberofExtensions(3);
        board.getTile(3).setNumberofExtensions(3);
        int expected =3;
        int actual = board.getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(board.getPlayerByName("Player 1, a")).size();
        assertEquals(expected,actual);

    }

    @Test
    void testdoesPlayerHaveAnyUpgradesToMaketrue() {
        Board board = setup3();
    }
    @Test
    void testdoesPlayerHaveAnyUpgradesToMakefalse() {
        Board board = setup3();
        assertFalse(board.doesPlayerHaveAnyUpgradesToMake(board.getPlayerByName("Player 2, b")));
    }
    @Test
    void testdoesPlayerHaveAnyUpgradesToMakeOverFour() {

        Board board = setup3();
        board.getTile(1).setNumberofExtensions(4);
        board.getTile(2).setNumberofExtensions(3);
        board.getTile(3).setNumberofExtensions(3);
        int expected =3;
        int actual = board.getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(board.getPlayerByName("Player 1, a")).size();
        assertTrue(board.doesPlayerHaveAnyUpgradesToMake(board.getPlayerByName("Player 1, a")));
        }

    @Test
    void testdoesPlayerHaveAnyUpgradesToMakeAllOverFour() {

        Board board = setup3();
        board.getTile(1).setNumberofExtensions(4);
        board.getTile(2).setNumberofExtensions(4);
        board.getTile(3).setNumberofExtensions(4);
        int expected =3;
        int actual = board.getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(board.getPlayerByName("Player 1, a")).size();
        assertFalse(board.doesPlayerHaveAnyUpgradesToMake(board.getPlayerByName("Player 1, a")));
    }
    @Test
    void testdoesPlayerHaveAnyUpgradesToMakeoneUnderThree() {

        Board board = setup3();
        board.getTile(1).setNumberofExtensions(3);
        board.getTile(2).setNumberofExtensions(3);
        board.getTile(3).setNumberofExtensions(2);
        int expected =3;
        int actual = board.getALltilesThatFinalUpgradeCanBeBuiltOnForPlayer(board.getPlayerByName("Player 1, a")).size();
        assertTrue(board.doesPlayerHaveAnyUpgradesToMake(board.getPlayerByName("Player 1, a")));
    }
    @Test
    void testgetPlayerByName() {
        Board board = setup3();
        Player actual =board.getPlayerByName("Player 2, b");
        assertEquals("Player 2, b",actual.getPlayername());
    }
    @Test
    void testgetPlayerByNameNull() {
        Board board = setup3();

        assertNull(board.getPlayerByName("Player 3, c"));
    }


}