package save.our.planet;


import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

public class ConsoleDeploymentTest {
//TODO write these tests



    public Board setup(){
        Board board = new Board();
        board.newGame();
        String [] names = new String[]{"james","josh"};
        board.setPlayerNames(names);
        Player james= board.getPlayerByName("Player "+1+", "+"james");
        james.setPlayerResources(5000);
        james.setPlayerPosition(1);
        board.getPlayerTilePosition(james).buyTile(james);
        james.setPlayerPosition(2);
        board.getPlayerTilePosition(james).buyTile(james);
        james.setPlayerPosition(3);
        board.getPlayerTilePosition(james).buyTile(james);
        Player josh= board.getPlayerByName("Player "+2+", "+"josh");
        josh.setPlayerResources(0);
        return board;
    }

    @Test
    public void testendOfGameScores() {
        ConsoleDeployment cd = new ConsoleDeployment();
        Board board = this.setup();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        cd.endOfGameScores(board);
        String actual = "\n" +
                "1: "+"Player "+2+", "+"josh"+" .Your final score is 0 ,\n" +
                "2: "+"Player "+1+", "+"james"+" .Your final score is 4820 ,";
        assertEquals(actual, outContent.toString());
    }

//    @Test
//    //todo make all of the inputs convert to lowercase
//    public void testupgradingOptions() {
////        ConsoleDeployment cd = new ConsoleDeployment();
////        Board board = setup();
////        String yes = "y";
////        ByteArrayInputStream incontent = new ByteArrayInputStream(yes.getBytes());
////        InputStream stdin = System.in;
////        System.setIn(incontent);
////        Scanner scanner = new Scanner(System.in);
////        System.setIn(stdin);
////        cd.upgradingOptions(scanner , board,board.getPlayerByName("james"));
//
//
//        fail();
//    }
//
//    @Test
//    public void upgradeExtensionsOrAreas() {
//        fail();
//    }
//
//    @Test
//    public void getAreasToBeFullyUpgraded() {
//        fail();
//    }
//
//    @Test
//    public void upgradeEitherExtensionsOrFinal() {
//        fail();
//    }
//
//    @Test
//    public void rollDice() {
//        fail();
//    }
//
//    @Test
//    public void yesOrno() {
//        fail();
//    }
//
//    @Test
//    public void landingoptions() {
//        fail();
//    }
//
//    @Test
//    public void landedOnActionableTile() {
//        fail();
//    }
//
//    @Test
//    public void nameEachPlayer() {
//        fail();
//
//    }

    @Test
    public void testCheckNumberOfPlayerstrue() {

        ConsoleDeployment deploy = new ConsoleDeployment();
        assertTrue(deploy.checkNumberOfPlayers(3));

    }
    @Test
    public void testCheckNumberOfPlayersFalse() {
        ConsoleDeployment deploy = new ConsoleDeployment();
        assertFalse(deploy.checkNumberOfPlayers(5));


    }
}