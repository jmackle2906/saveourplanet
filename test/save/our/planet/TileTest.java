package save.our.planet;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TileTest extends Tile {

    public Tile tilesetup(){
        Tile tile = new Tile();
        tile.setPrice(50);
        Player player = new Player();
        player.setPlayerResources(100);
        tile.buyTile(player);
        tile.buildExtension();
        tile.addNameAndIndex("tile", 1);
        return tile;
    }


    @Test
    public void testGetCostOfNExtUpgrade(){
        Tile tile =this.tilesetup();
        int expected = 10;
        int actual = tile.getCostOfNextUpgrade();
        System.out.println(actual);
        assertEquals(expected,actual);

    }
    @Test
    public void testbuildExtensionAbove3(){
        Tile tile =this.tilesetup();
        tile.buildExtension();
        tile.buildExtension();

        int expected = 40;
        int actual = tile.getCostOfNextUpgrade();
        System.out.println(actual);
        assertEquals(expected,actual);

    }

    @Test
    public void testbuildExtensionAbove4(){
        Tile tile =this.tilesetup();
        tile.buildExtension();
        tile.buildExtension();
        tile.buildExtension();
        //int actual =tile.getCostOfNextUpgrade();
        int expected = 0;
        int actual = tile.getCostOfNextUpgrade();
        System.out.println(actual);
        assertEquals(expected,actual);

    }
    @Test
    public void testChargeNonOwner(){
        Tile tile =this.tilesetup();
        int expected =30;
        int actual = tile.getPriceforNoneOwners();
        assertEquals(expected,actual);

    }
    @Test
    public void testChargeNoneOwner(){
        Tile tile =this.tilesetup();
        int expected=70;
        Player player =new Player();
        player.setPlayerResources(100);
        tile.chargeNoneOwner(player);
        int actual = player.getPlayerResources();
        System.out.println(actual);
        assertEquals(expected,actual);

    }
    @Test
    public void testGetExtensionCount(){
        Tile tile = this.tilesetup();
        int actual =tile.getExtensionCount();
        int expected = 1;
        assertEquals(actual,expected);

    }

}