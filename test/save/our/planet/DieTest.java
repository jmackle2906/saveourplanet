package save.our.planet;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DieTest {

    @Test
    public void testRollDieAllwaysReturnsNumberBetween1and6() {
        for (int i = 0; i < 5000; i++) {
            int rollResult= Die.roll();
            assertTrue( rollResult <= 6);
            assertTrue( rollResult >= 1);
        }
    }
}
